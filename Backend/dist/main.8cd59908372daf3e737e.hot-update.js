exports.id = "main";
exports.modules = {

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var apollo_server_1 = __webpack_require__(/*! apollo-server */ "apollo-server");
var resolvers_1 = __importDefault(__webpack_require__(/*! ./resolvers */ "./src/resolvers.ts"));
var typeDefs_1 = __importDefault(__webpack_require__(/*! ./typeDefs */ "./src/typeDefs.ts"));
var server = new apollo_server_1.ApolloServer({
    resolvers: resolvers_1.default,
    typeDefs: typeDefs_1.default,
    formatError: function (error) {
        console.warn(error);
        return error;
    },
});
server.listen().then(function (_a) {
    var url = _a.url;
    return console.log("Server ready at " + url + ". ");
});
if (true) {
    module.hot.accept();
    module.hot.dispose(function () { return console.log('Module disposed. '); });
}


/***/ }),

/***/ "./src/typeDefs.ts":
/*!*************************!*\
  !*** ./src/typeDefs.ts ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
Object.defineProperty(exports, "__esModule", { value: true });
var apollo_server_1 = __webpack_require__(/*! apollo-server */ "apollo-server");
exports.default = apollo_server_1.gql(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  type Task {\n    value: String\n    isChecked: Boolean\n  }\n  input TaskInput{\n    value: String\n    isChecked: Boolean\n  }\n  type Query {\n    tasks(id:String):[Task]\n    task(index:Int!):Task\n    find: [Task]\n  }\n  type Mutation{\n    update(tasks:[TaskInput]): [Task]\n  }\n\n"], ["\n  type Task {\n    value: String\n    isChecked: Boolean\n  }\n  input TaskInput{\n    value: String\n    isChecked: Boolean\n  }\n  type Query {\n    tasks(id:String):[Task]\n    task(index:Int!):Task\n    find: [Task]\n  }\n  type Mutation{\n    update(tasks:[TaskInput]): [Task]\n  }\n\n"])));
var templateObject_1;


/***/ })

};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvbWFpbi50cyIsIndlYnBhY2s6Ly8vLi9zcmMvdHlwZURlZnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBYTtBQUNiO0FBQ0EsNENBQTRDO0FBQzVDO0FBQ0EsOENBQThDLGNBQWM7QUFDNUQsc0JBQXNCLG1CQUFPLENBQUMsb0NBQWU7QUFDN0Msa0NBQWtDLG1CQUFPLENBQUMsdUNBQWE7QUFDdkQsaUNBQWlDLG1CQUFPLENBQUMscUNBQVk7QUFDckQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0QsSUFBSSxJQUFVO0FBQ2Q7QUFDQSxvQ0FBb0MseUNBQXlDLEVBQUU7QUFDL0U7Ozs7Ozs7Ozs7Ozs7QUN2QmE7QUFDYjtBQUNBLGdDQUFnQyx1Q0FBdUMsYUFBYSxFQUFFLEVBQUUsT0FBTyxrQkFBa0I7QUFDakg7QUFDQTtBQUNBLDhDQUE4QyxjQUFjO0FBQzVELHNCQUFzQixtQkFBTyxDQUFDLG9DQUFlO0FBQzdDLG9IQUFvSCxnREFBZ0Qsb0JBQW9CLGdEQUFnRCxnQkFBZ0IsK0VBQStFLGtCQUFrQiw0Q0FBNEMseUJBQXlCLGdEQUFnRCxvQkFBb0IsZ0RBQWdELGdCQUFnQiwrRUFBK0Usa0JBQWtCLDRDQUE0QztBQUMvcUIiLCJmaWxlIjoibWFpbi44Y2Q1OTkwODM3MmRhZjNlNzM3ZS5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiXCJ1c2Ugc3RyaWN0XCI7XG52YXIgX19pbXBvcnREZWZhdWx0ID0gKHRoaXMgJiYgdGhpcy5fX2ltcG9ydERlZmF1bHQpIHx8IGZ1bmN0aW9uIChtb2QpIHtcbiAgICByZXR1cm4gKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgPyBtb2QgOiB7IFwiZGVmYXVsdFwiOiBtb2QgfTtcbn07XG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHsgdmFsdWU6IHRydWUgfSk7XG52YXIgYXBvbGxvX3NlcnZlcl8xID0gcmVxdWlyZShcImFwb2xsby1zZXJ2ZXJcIik7XG52YXIgcmVzb2x2ZXJzXzEgPSBfX2ltcG9ydERlZmF1bHQocmVxdWlyZShcIi4vcmVzb2x2ZXJzXCIpKTtcbnZhciB0eXBlRGVmc18xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCIuL3R5cGVEZWZzXCIpKTtcbnZhciBzZXJ2ZXIgPSBuZXcgYXBvbGxvX3NlcnZlcl8xLkFwb2xsb1NlcnZlcih7XG4gICAgcmVzb2x2ZXJzOiByZXNvbHZlcnNfMS5kZWZhdWx0LFxuICAgIHR5cGVEZWZzOiB0eXBlRGVmc18xLmRlZmF1bHQsXG4gICAgZm9ybWF0RXJyb3I6IGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICBjb25zb2xlLndhcm4oZXJyb3IpO1xuICAgICAgICByZXR1cm4gZXJyb3I7XG4gICAgfSxcbn0pO1xuc2VydmVyLmxpc3RlbigpLnRoZW4oZnVuY3Rpb24gKF9hKSB7XG4gICAgdmFyIHVybCA9IF9hLnVybDtcbiAgICByZXR1cm4gY29uc29sZS5sb2coXCJTZXJ2ZXIgcmVhZHkgYXQgXCIgKyB1cmwgKyBcIi4gXCIpO1xufSk7XG5pZiAobW9kdWxlLmhvdCkge1xuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uICgpIHsgcmV0dXJuIGNvbnNvbGUubG9nKCdNb2R1bGUgZGlzcG9zZWQuICcpOyB9KTtcbn1cbiIsIlwidXNlIHN0cmljdFwiO1xudmFyIF9fbWFrZVRlbXBsYXRlT2JqZWN0ID0gKHRoaXMgJiYgdGhpcy5fX21ha2VUZW1wbGF0ZU9iamVjdCkgfHwgZnVuY3Rpb24gKGNvb2tlZCwgcmF3KSB7XG4gICAgaWYgKE9iamVjdC5kZWZpbmVQcm9wZXJ0eSkgeyBPYmplY3QuZGVmaW5lUHJvcGVydHkoY29va2VkLCBcInJhd1wiLCB7IHZhbHVlOiByYXcgfSk7IH0gZWxzZSB7IGNvb2tlZC5yYXcgPSByYXc7IH1cbiAgICByZXR1cm4gY29va2VkO1xufTtcbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwgeyB2YWx1ZTogdHJ1ZSB9KTtcbnZhciBhcG9sbG9fc2VydmVyXzEgPSByZXF1aXJlKFwiYXBvbGxvLXNlcnZlclwiKTtcbmV4cG9ydHMuZGVmYXVsdCA9IGFwb2xsb19zZXJ2ZXJfMS5ncWwodGVtcGxhdGVPYmplY3RfMSB8fCAodGVtcGxhdGVPYmplY3RfMSA9IF9fbWFrZVRlbXBsYXRlT2JqZWN0KFtcIlxcbiAgdHlwZSBUYXNrIHtcXG4gICAgdmFsdWU6IFN0cmluZ1xcbiAgICBpc0NoZWNrZWQ6IEJvb2xlYW5cXG4gIH1cXG4gIGlucHV0IFRhc2tJbnB1dHtcXG4gICAgdmFsdWU6IFN0cmluZ1xcbiAgICBpc0NoZWNrZWQ6IEJvb2xlYW5cXG4gIH1cXG4gIHR5cGUgUXVlcnkge1xcbiAgICB0YXNrcyhpZDpTdHJpbmcpOltUYXNrXVxcbiAgICB0YXNrKGluZGV4OkludCEpOlRhc2tcXG4gICAgZmluZDogW1Rhc2tdXFxuICB9XFxuICB0eXBlIE11dGF0aW9ue1xcbiAgICB1cGRhdGUodGFza3M6W1Rhc2tJbnB1dF0pOiBbVGFza11cXG4gIH1cXG5cXG5cIl0sIFtcIlxcbiAgdHlwZSBUYXNrIHtcXG4gICAgdmFsdWU6IFN0cmluZ1xcbiAgICBpc0NoZWNrZWQ6IEJvb2xlYW5cXG4gIH1cXG4gIGlucHV0IFRhc2tJbnB1dHtcXG4gICAgdmFsdWU6IFN0cmluZ1xcbiAgICBpc0NoZWNrZWQ6IEJvb2xlYW5cXG4gIH1cXG4gIHR5cGUgUXVlcnkge1xcbiAgICB0YXNrcyhpZDpTdHJpbmcpOltUYXNrXVxcbiAgICB0YXNrKGluZGV4OkludCEpOlRhc2tcXG4gICAgZmluZDogW1Rhc2tdXFxuICB9XFxuICB0eXBlIE11dGF0aW9ue1xcbiAgICB1cGRhdGUodGFza3M6W1Rhc2tJbnB1dF0pOiBbVGFza11cXG4gIH1cXG5cXG5cIl0pKSk7XG52YXIgdGVtcGxhdGVPYmplY3RfMTtcbiJdLCJzb3VyY2VSb290IjoiIn0=