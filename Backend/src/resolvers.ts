import mongoQueries from './mongoQueries';
const queries = new mongoQueries();

export default {
    Query: {
      find: async() => queries.findTask(),
    },
    Mutation:{
          update: async(__:any, params:TaskParams) =>{
             const { tasks } = params;
             return queries.updateTask(tasks)
          }
    }
  };

type TaskParams = {
  tasks:Task[]
}

type Task = {
  value: string
  isChecked: boolean
}
