import { gql } from 'apollo-server';

export default gql`
  type Task {
    value: String
    isChecked: Boolean
  }
  input TaskInput{
    value: String
    isChecked: Boolean
  }
  type Query {
    tasks(id:String):[Task]
    task(index:Int!):Task
    find: [Task]
  }
  type Mutation{
    update(tasks:[TaskInput]): [Task]
  }

`;
