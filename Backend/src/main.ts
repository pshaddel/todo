import { ApolloServer } from 'apollo-server';

import resolvers from './resolvers';
import typeDefs from './typeDefs';

const server = new ApolloServer({
   resolvers, 
   typeDefs,
   formatError: error => {
    console.warn(error);
    return error;
  },
  // formatResponse: (response, query) => {
  //   console.info('GraphQL query and variables', {
  //     query: query.queryString,
  //     vars: query.variables,
  //   });
  // return response;
// }, 
});

server.listen().then(({ url }) => console.log(`Server ready at ${url}. `));
if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => console.log('Module disposed. '));
}