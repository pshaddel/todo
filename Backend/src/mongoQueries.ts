import connection from './mongo'
import { MongoClient } from 'mongodb'

export default class MongoQueries {

    async findTask() {
        const client = await connection();
        console.log("find...")
        const db = client.db("TODO")
        const data = await db.collection("Tasks").findOne({user:"default"});
        client.close()
        return (data.Tasks ? data.Tasks : [])
    }

    async updateTask(tasks: Task[]) {
        const client = await connection();
        const db = client.db("TODO")
        await db.collection("Tasks").updateOne({user:"default"}, {$set:{Tasks: tasks}});
        console.log("update...")
        client.close()
        return tasks
    }
} 

type Task = {
    value: string
    isChecked: boolean
  }