import { MongoClient } from 'mongodb';
const url = "mongodb://localhost:27020";

const connection = async()=>{
    const client:MongoClient = await MongoClient.connect(url)
    return client
}
export default connection