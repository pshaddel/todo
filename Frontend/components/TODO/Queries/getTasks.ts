import gql from 'graphql-tag';
export default gql`
query find {
  find {
    value
    isChecked
  }
}
`;
