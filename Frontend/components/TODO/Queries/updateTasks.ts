import gql from 'graphql-tag';
export default gql`
mutation MYquery($newTasks: [TaskInput])
{
  update(tasks: $newTasks){
    value
    isChecked
  }
}
`;
