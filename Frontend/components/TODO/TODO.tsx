import React, { FC, useState, useEffect } from "react";
import { useQuery, useMutation } from '@apollo/react-hooks';
import TextField from "@material-ui/core/TextField";
import Task from "./Task";
import styles from "./TODO.module.css";
import getTasksGQL from './Queries/getTasks'
import updateTasksGQL from './Queries/updateTasks'

export const TODO: FC<{}> = () => {
  const defaultTask:Task = { value: "", isChecked: false };
  const [currentTask, setCurrentTask] = useState(defaultTask);
  const name   = useInputForm("Poorshad");
  const family = useInputForm("Shaddel");

  const { error, data, refetch } = useQuery(getTasksGQL);

  if(error){
    return <div>
      Sorry, Something Went Wrong:<br/>
      {error.toString()}
    </div>
  }

  const tasks = (data) ? data.find.map(val=>{return {value: val.value, isChecked: val.isChecked}}) : []
  const [mutateTasks] = useMutation(updateTasksGQL)
  const changeTasks = (newTasks) =>{
    mutateTasks({ variables: { newTasks }});
    refetch();
  }

  const handleCurrentTaskChange = (e) => setCurrentTask({ value: e.target.value, isChecked: false });
  const handleAddTask = () => {
    if (currentTask) {
      changeTasks([...tasks, currentTask]);
      setCurrentTask(defaultTask);
    }
  };

  const handleKeyPress = (event) => {if (event.key === "Enter") handleAddTask();};
  
  const removeTask = (index: number) => {
    let newTasks = [...tasks];
    newTasks.splice(index, 1);
    changeTasks(newTasks);
  };

  const onCheck = (index) => {
    let newTasks = [...tasks];
    newTasks[index].isChecked = !newTasks[index].isChecked;
    changeTasks(newTasks);
  };

  return (
    <>
      <p className={styles.title}>
        MY FIRST TODOAPP - NEXT.JS/TYPESCRIPT/REACT
      </p>
      <div className={styles.container}>
        <TextField
          placeholder="Enter Your Family Here..."
          id="Name"
          value={name.value}
          onChange={name.handleChange}
          label="Name"
          onKeyPress={handleKeyPress}
          className={styles.taskText}
          />
        <TextField
          placeholder="Enter Your Name Here..."
          id="Family"
          value={family.value}
          onChange={family.handleChange}
          label="Family"
          className={styles.taskText}
          />

        <TextField
          required={true}
          fullWidth={true}
          placeholder="Enter Your Task Here..."
          id="CurrentTask"
          value={currentTask.value}
          onChange={handleCurrentTaskChange}
          label="Your Task"
          onKeyPress={handleKeyPress}
          className={styles.taskText}
          />
          <p className={styles.containerTitle}>{name.value + " " + family.value}</p>
        <div className={styles.todoTasks}>
          <p className={styles.containerTitle}>TODO</p>
          <TaskList  condition={false} tasks={tasks} removeTask={removeTask} onCheck={onCheck} />
        </div>
        <div className={styles.doneTasks}>
          <p className={styles.containerTitle}>DONE</p>
          <TaskList  condition={true} tasks={tasks} removeTask={removeTask} onCheck={onCheck} />
        </div>
      </div>
    </>
  );
};

type Task = {
  value: string,
  isChecked: boolean
}

type inputValue = {
  target: {
    value: string
  }
}

type inputFormOutput = {
  value: string,
  handleChange: (e: inputValue)=>void
}

const useInputForm = (init:string):inputFormOutput =>{
  const [value, setValue] = useState(init);
  const handleChange = (e: inputValue)=>{
    setValue(e.target.value)
  }
  return {
    value,
    handleChange
  }
}

const TaskList:FC<{tasks, condition, removeTask, onCheck}> = (props)=>{
  const { condition, tasks, removeTask, onCheck } = props;
  return tasks.map((task, index) => {
    if (task.isChecked === condition)
      return (
        <Task
          key={condition.toString()+index}
          id={index}
          description={task.value}
          removeTask={removeTask.bind(this, index)}
          isChecked={task.isChecked}
          onCheck={onCheck.bind(this, index)}
        />
      );
    else return null;
  })
}