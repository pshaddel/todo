import React from 'react'
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import styles from "./Task.module.css";

type TASK = {
    id: number,
    description: string,
    removeTask: (index: number)=>void, 
    onCheck: (index: number)=>void
    isChecked: boolean
  }

  const Task = (props: TASK) => {
    const { id, isChecked, description, removeTask, onCheck } = props;
    const itemId: string = id.toString();
    return (
      <ListItem id={itemId} key={itemId} role={undefined} dense button>
        <Checkbox id={itemId} edge="start" onChange={onCheck.bind(this, id)} checked={isChecked} />
        <ListItemText style={{textDecoration:(isChecked)? "line-through":"none"}} id={itemId} primary={description} />
        <IconButton id={itemId} edge="end" aria-label="remove" onClick={removeTask.bind(this, id)}>
          <DeleteIcon className={styles.removeTaskIcon} id={itemId} />
        </IconButton>
      </ListItem>
    );
  };
  
  export default Task;